<?php

return [

    'allow_login'           => 'Permitir Acesso?',
    'user_created'          => 'Usuário criado',
    'user_birthday'          => 'Data de Nascimento',

    'error' => [
        'email'             => 'Este e-mail já foi utilizado.'
    ],

    'notification' => [
        'message'       => ':customer fez o pagamento de :amount para a fatura de numero :invoice_number.',
        'button'        => 'Mostrar',
    ],
];
