<?php

return [

    'version'               => 'Versão',
    'powered'               => 'Desenvolvido por Vital Solutions',
    'link'                  => 'https://vitalsolutions.com.br',
    'software'              => 'Software de Gestão',

];
