<?php

return [

    'title' => 'Paypal',
    'paypalstandard' => 'Paypal',

    'form' => [
        'email' => 'Email',
        'mode' => 'Modo',
        'debug' => 'Debug',
        'transaction' => 'Transação',
        'customer' => 'Exibir para o Cliente',
        'order' => 'Ordem',
    ],

    'test_mode' => 'Aviso: o gateway de pagamento está em \'Modo Sandbox\'. A sua conta não será cobrada.',
    'description' => 'Pagar com Paypal',
    'confirm' => 'Confirmar',

];
