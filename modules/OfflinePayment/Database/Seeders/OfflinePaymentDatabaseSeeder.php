<?php

namespace Modules\OfflinePayment\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class OfflinePaymentDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->create();

        Model::reguard();
    }

    private function create()
    {
        $company_id = $this->command->argument('company');

        setting()->setExtraColumns(['company_id' => $company_id]);

        $methods = [];

        $methods[] = [
            'code' => 'offlinepayment.cash.1',
            'name' => 'Dinheiro',
            'customer' => '0',
            'order' => '1',
            'description' => null,
        ];

        $methods[] = [
            'code' => 'offlinepayment.credit.2',
            'name' => 'Cartão de Crédito',
            'customer' => '0',
            'order' => '2',
            'description' => null,
        ];

        $methods[] = [
            'code' => 'offlinepayment.debit.3',
            'name' => 'Cartão de Débito',
            'customer' => '0',
            'order' => '3',
            'description' => null,
        ];

        $methods[] = [
            'code' => 'offlinepayment.transfer.4',
            'name' => 'Transferência Bancária',
            'customer' => '0',
            'order' => '4',
            'description' => null,
        ];

        setting()->set('offlinepayment.methods', json_encode($methods));
    }
}
