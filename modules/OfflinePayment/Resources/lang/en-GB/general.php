<?php

return [

    'title'   => 'Formas de Pagamento',

    'add_new' => 'Novo',
    'edit' => 'Edit: :method',

    'form' => [
        'code' => 'Código',
        'customer' => 'Exibir para o Cliente',
        'order' => 'Ordem'
    ],

    'payment_gateways' => 'Métodos de Pagamento',

    'confirm' => 'Confirmar',
    'loading' => 'Carregando',

];
